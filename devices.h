/**********************************************************************
 * @file   devices.h
 * @brief  Device and sensor structures
 *
 * Copyright(C) 2021, Cumali Ceylan
 * All rights reserved.
 ***********************************************************************
 *
 *
 **********************************************************************/
#ifndef DEVICES_H_
#define DEVICES_H_

#include <string>

/// Basic device class
class DEVICE
{
public:

    DEVICE()
    {
        this->message_count = 0;
    }

    DEVICE(const std::string name, const std::string path)
        : name(name), path(path)
    {
        this->message_count = 0;
    }

    // Return name of the device
    std::string get_name()
    {
        return(this->name);
    }

    // Return name of the device
    std::string get_path()
    {
        return(this->path);
    }

    // Return path of the device
    std::string get_dev_path()
    {
        return(path);
    }

    // Get handle of the device
    int get_handle()
    {
        return(handle);
    }

    // Return count of messages the device received
    unsigned long long get_message_count()
    {
        return(message_count);
    }

    void operator++()
    {
        this->message_count++;
    }

    DEVICE operator++(int)
    {
        DEVICE dev;
        this->message_count++;
        dev.message_count = this->message_count;
        return(dev);
    }

    // Initialize device
    void init_device();

private:

    // Name of the device
    std::string name;

    // Path of the device
    std::string path;

    // Handle of the device
    int handle;

    // Number of the messages that device has sent
    unsigned long long int message_count;

};

/// Base class for sensor devices
class SENSOR : public DEVICE
{
public:

    SENSOR(const std::string name, const std::string device_path)
        : DEVICE(name, device_path)
    {
        ;
    }
};

/// Temperature sensors
class TEMPERATURE : public SENSOR
{
public:

    TEMPERATURE(const std::string name, const std::string device_path)
        : SENSOR(name, device_path)
    {
        ;
    }
};

/// Humidity sensors
class HUMIDITY : public SENSOR
{
public:

    HUMIDITY(const std::string name, const std::string device_path)
        : SENSOR(name, device_path)
    {
        ;
    }
};

#endif // DEVICES_H_
