/**********************************************************************
 * @file   devices.cc
 * @brief
 *
 * Copyright(C) 2021, Cumali Ceylan
 * All rights reserved.
 ***********************************************************************
 *
 *
 **********************************************************************/
#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <termios.h>

#include "devices.h"
#include "lmexp.h"

/**
 * Initialize serial port the device has been connected
 *
 */
void DEVICE::init_device()
{
    this->handle = open(this->path.c_str(), O_RDWR | O_NOCTTY);
    if (this->handle == -1)
    {
        char errstr[128];
        throw LMEXP("Unable to open serial device (%s), errno = %d(%s)",
                    this->path.c_str(), errno, strerror_r(errno, errstr, sizeof(errstr)));
    }

    fcntl(this->handle, F_SETFL, 0);

    // Get default options
    struct termios options;
    tcgetattr(this->handle, &options);

    // Adjust baud rate
    cfsetispeed(&options, B115200);
    cfsetospeed(&options, B115200);

    // Disable hardware-software flow control
    options.c_cflag &= ~CRTSCTS;
    options.c_iflag &= ~(IXON | IXOFF | IXANY | INLCR | ICRNL);

    // Choose raw input/ouptput
    options.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);
    options.c_oflag &= ~OPOST;

    // Set new options
    tcsetattr(this->handle, TCSANOW, &options);
}
