CC=g++
CFLAGS=-I.
OBJS = \
	main.o \
	devices.o

%.o: %.cc
	$(CC) -c -o $@ $< $(CFLAGS)

all: $(OBJS)
	$(CC) -o lm $(OBJS) -lpthread

.PHONY: clean

clean:
	-rm lm *.o
