/**********************************************************************
 * @file   main.cc
 * @brief  Main module
 *
 * Copyright(C) 2022, Cumali Ceylan
 * All rights reserved.
 ***********************************************************************
 *
 *
 **********************************************************************/
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <pthread.h>

#include "devices.h"
#include "lmexp.h"


/// List of devices can be read.
/// Notes: I hard coded 2 device here,
/// if you want to test more device, you should enable commented out
/// lines and also create virtual ports for them as defined in README
/// file
static DEVICE device_list[] =
    {
     TEMPERATURE("Temp1", "LM1"),
     TEMPERATURE("Temp2", "LM3"),
     // HUMIDITY("Hum1", "LM5"),
     // HUMIDITY("Hum2", "LM7"),
    };

/// Thread ID
static pthread_t worker_thread_id;

/**
 * Serial port worker thread. Reads data from devices
 *
 * @param arg Argument to the thread
 *
 * @return
 */
void *worker_thread(void *arg)
{
    printf("arg = %p, tid = %d\n", arg, gettid());

    try
    {
        // Serial listening infinite loop
        while (1)
        {
            // Set 5000 ms timeout
            timeval tv;
            tv.tv_sec  = 0;
            tv.tv_usec = (5000 * 1000);

            // Set serial port handles
            fd_set readfs;
            FD_ZERO(&readfs);
            int biggest_handle = device_list[0].get_handle();
            for(unsigned int index = 0; index < sizeof(device_list) / sizeof(device_list[0]); index++)
            {
                // Set handle
                FD_SET(device_list[index].get_handle(), &readfs);

                // Update biggest handle number if this handle bigger
                // than it.
                if(device_list[index].get_handle() > biggest_handle);
                {
                    biggest_handle = device_list[index].get_handle();
                }
            }

            // Select until any command has been received
            int rc = select(biggest_handle + 1, &readfs, 0, 0, &tv);
            if (rc == -1)
            {
                char errmsg[128];
                throw LMEXP("select() failed, errno=%d(%s)", errno, strerror_r(errno, errmsg, sizeof(errmsg)));
            }
            else if (rc == 0)
            {
            }
            else
            {
                // Find the serial port data received
                for(unsigned int index = 0; index < sizeof(device_list) / sizeof(device_list[0]); index++)
                {
                    if (FD_ISSET(device_list[index].get_handle(), &readfs) != 0)
                    {
                        // Read data
                        char data[128];
                        data[0] = '\0';
                        rc = read(device_list[index].get_handle(), data, sizeof(data));
                        if(rc != -1)
                        {
                            device_list[index]++;
                            printf("name = %s, message_count = %llu, data = %s\n",
                                   device_list[index].get_name().c_str(),
                                   device_list[index].get_message_count(),
                                   data);
                        }
                    }
                }
            }
        }
    }
    catch(const _LMEXP& exp)
    {
        char msg[256];
        snprintf(msg, sizeof(msg), "*** EXCEPTION msg = %s", exp.getmsg());
        printf("%s\n", msg);
    }

    return(0);
}

/**
 * Main function
 *
 *
 * @return
 */
int main()
{
    try
    {
        // Initialize devices
        for(unsigned int index = 0; index < sizeof(device_list) / sizeof(device_list[0]); index++)
        {
            device_list[index].init_device();

            printf("Initialized device, name = %s, path=%s\n",
                   device_list[index].get_name().c_str(),
                   device_list[index].get_path().c_str());
        }

        int rc = pthread_create(&worker_thread_id, 0, worker_thread, 0);
        if(rc != 0)
        {
            throw LMEXP("Could not create worker thread, errno = %d", errno);
        }

        pause();
    }
    catch(const _LMEXP& exp)
    {
        char msg[256];
        snprintf(msg, sizeof(msg), "*** EXCEPTION msg = %s\n", exp.getmsg());
        printf("%s\n", msg);
    }
}
