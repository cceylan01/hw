/**********************************************************************
 * @file   lmexp.h
 * @brief  Exception class for the project
 *
 * Copyright(C) 2022, Cumali Ceylan
 * All rights reserved.
 ***********************************************************************
 *
 *
 **********************************************************************/
#ifndef LMEXP_H_
#define LMEXP_H_

#include <stdio.h>
#include <stdarg.h>
#include <string>

/// Listen message exception class
class _LMEXP
{
public:

    _LMEXP(const char* file, int line, const char* format, ...)
    {
        // Build string with format
        char msg[1024];
        int rc = snprintf(msg, sizeof(msg), "FILE:%s, LINE:%d - ", file, line);
        va_list argptr;
        va_start(argptr, format);
        vsnprintf(msg + rc, sizeof(msg) - rc, format, argptr);
        va_end(argptr);

        _LMEXP::msg = std::string(msg);
    }

    const char* getmsg() const { return(msg.c_str()); }

private:
    std::string msg;
};

#define LMEXP(format, ...) _LMEXP(__FILE__, __LINE__, format, ##__VA_ARGS__)

#endif
