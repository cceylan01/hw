# Message Listening Homework (lm)

The lm application listens virtual serial ports and counts messages
for each device defined in the source code.

## Dependencies

The application has been prepared on an Ubuntu Linux machine. In order
to compile it, GCC and make utility are needed. In order to test it,
also socat utility should be installed on test PC.

## Compiling

Below command compiles source codes and output lm executable.

$ make clean all

## Usage

In order to test this application, 2 virtual serial ports (because
there are 2 device in the source file, if you want to change it, read
the notes of the device list definition in the source file) should be
created by issuing below lines from two different shell screens.

$ socat -d -d pty,raw,echo=0,link=LM0 pty,raw,echo=0,link=LM1

  This line creates 2 virtual serial port. LM0 is used to send data to
  first device and LM1 is used by the lm application to read data as
  defined in 26. line of the main.cc

$ socat -d -d pty,raw,echo=0,link=LM2 pty,raw,echo=0,link=LM3

  This line creates 2 virtual serial port. LM2 is used to send data to
  second device and LM3 is used by the lm application to read data as
  defined in 26. line of the main.cc

After this, lm application can be run by directly writing below command.

$ ./lm

Now, we can run test.sh script file. It will be send data in an
infinite loop.

$ ./test.sh

After these steps, lm will print received messages and message counts.

Note 1: Every operations in this documents are done in the same
directory with the source.

Note 2: Assumed that your user is in "dialout" group. Otherwise please
add it.


