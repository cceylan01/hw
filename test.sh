#!/bin/bash
for (( ; ; ))
do
    echo "Sending data throughout LM0 - LM1"
    echo "Test data for LM1 listener" > LM0
    sleep 1
    echo "Sending data throughout LM2 - LM3"
    echo "Test data for LM3 listener" > LM2
    sleep 1
done
